#!/bin/sh
cd `dirname "$_"`

set -e
set -x

if [ ! -e /opt/gitlab-kitainer/var/is-not-first-run ] ; then
    echo "$0: COPYING GITLAB DATA INTO POSITION"
    mkdir -p /etc/ /var/log/ /var/opt/
    cd /
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/etc.tgz
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/log.tgz
    tar --same-owner -zxpvf /opt/gitlab-kitainer/gitlab-data/opt.tgz

    mkdir -p /opt/gitlab-kitainer/var
    touch /opt/gitlab-kitainer/var/is-not-first-run
fi

echo "$0: STARTING GITLAB"
/assets/wrapper

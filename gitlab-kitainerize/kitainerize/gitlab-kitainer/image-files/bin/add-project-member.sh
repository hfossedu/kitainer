#!/bin/sh

TOKEN=$1
PROJECT_ID=$2
USER_ID=$3
ROLE_VALUE=$4

curl --request POST \
    --header "PRIVATE-TOKEN: $TOKEN" \
    --data "user_id=$USER_ID&access_level=$ROLE_VALUE" \
    https://gitlab.example.com/api/v4/projects/$PROJECT_ID/members

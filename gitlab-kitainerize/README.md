# GitLab-Kitainerize

v0.3.0 - 2020-02-14

Given an exported GitLab project, creates a Docker container that that contains GitLab loaded with the project and some default accounts.

## What's New

* v0.3.1 - 2020-02-25
  * Fix: Increasing selenium timeouts to increase reliability.
  * Fix: Ensuring mkdir command does not fail in the wrapper that starts the kitainer.
  * Doc: Improve documentation.
* v0.3.0 - 2020-02-14
  * Add: parameters for `GITLAB_IMAGE` and `KITAINER_IMAGE`.
  * Add: selenium timeouts to improve resilience.
  * Add: Store access tokens for root and owner00 in image.
  * Refactor: Pay down technical debt.
* v0.2.0 - 2020-02-02 - Fully automated scripts that create a GitLab-Kitainer.
* v0.1.0 - 2020-01-26 - Set of scripts that can be used to create a GitLab-Kitainer.

## Requires

Version numbers are those used to build and test this project. It may work with other versions.

* sh (3.2.57) (/bin/sh)
* Docker (2.2.0)
* Python (3.8)
* Selenium chrome drivers (79.0.3945.36)
* Selenium for Python (3.141.0)
* At least 4GB of memory allocated to Docker

## Install Chrome Drivers for Selenium

Download the chrome driver from <https://sites.google.com/a/chromium.org/chromedriver/downloads> and put it in the current directory.

On Mac OS X(what's used here..) you can use brew: `brew cask install chromedriver`

## Install Selenium for Python

Assuming Python 3 is your default python version (so pip will install to Python 3 libraries)...

```bash
pip install -r requirements.txt
```

## Build a GitLab Kitainer for a Project

1. If project is in a service other than GitLab, import it into GitLab.

2. Use GitLab to export the project to a zip file.

3. Build kitainer

    ```bash
    ./src/kitainerize GITLAB_IMAGE KITAINER_IMAGE EXPORTED_GITLAB_ZIPFILE PROJECT_NAME
    ```

    This may take a long time. 10-15 minutes is not unreasonable.

4. Run kitainer

    ```bash
    docker run --detach \
      --hostname localhost \
      --publish 443:443 --publish 80:80 --publish 22:22 \
      --name kitainer \
      --restart always \
      KITAINER_IMAGE
    ```

    Open a browser to <http://localhost/> . Login using one of accounts given below.

5. Stop kitainer

    ```bash
    docker stop kitainer
    ```

6. Start the stopped kitainer

    ```bash
    docker start kitainer
    ```

7. Remove the stopped kitainer

    ```bash
    docker rm kitainer
    ```

8. Remove the kitainer image (after stopping and removing the kitainer)

    ```bash
    docker rmi KITAINER_IMAGE
    ```

The kitainer is built on top of Docker. So all concepts related to Docker, apply to Kitainers. For example, Kitainers retainer their state when they are stopped and then started again, as long as they are not removed. If they are removed, and a new kitainer is started based on its original image, it starts with the original state, effectively resetting the kitainer.

GitLab Kitainers are based on GitLab's Docker image. To persist data across the removal of one kitainer and the start of another, see GitLab's documentation for starting a container using volume mounts (<https://docs.gitlab.com/omnibus/docker/)>).

## Accounts

The kitainer has the following accounts

| Username | Password |
| -------- | -------- |
| root     | rootroot |
| owner00  | owner00owner00 |

## Acknowledgements

This was a combined effort of
[Grant Braught](https://www.dickinson.edu/site/custom_scripts/dc_faculty_profile_index.php?fac=braught),
[Karl Wurst](http://cs.worcester.edu/kwurst/), and
[Stoney Jackson](http://wne.edu/~hjackson).

Thanks to Kelly Hair's for licensing his work [get-gitlab-token](https://gitlab.com/khair1/get-gitlab-token) under MIT License.

## Developer Notes

### GitLab-Kitainerize Overview

GitLab-Kitainerize builds a Docker image that contains a GitLab instance that has one project imported and a user account that owns them. It does this as follows.

1. It runs a GitLab container setting the root password as it does. The password is hard-coded as an environment variable in a docker-compose.yml file. The version of GitLab is pinned in this file too.
2. It creates and retrieves an access token for root. This is done using a Python3 script that in turn uses Chrome, a Chrome WebDriver, and Selenium to interact with the running GitLab UI. Hard-coded into this script is the root password mentioned in the previous step.
3. It creates a user account to own the imported projects, Owner00. The password is hard-coded into the script that creates it. This is done using curl to interact with GitLab's REST-API using the root access token.
4. It creates and retrieves an access token for Owner00. This is done using a Python3 script that in turn uses Chrome, a Chrome WebDriver, and Selenium to interact with the running GitLab UI. Hard-coded into this script is the password mentioned in the previous step.
5. It imports the project into GitLab, and then waits until the import is done. The user, when they invoke gitlab-kitainerize, supplies the project name and a path to a file that is a project exported from GitLab. This import is done using curl to interact with GitLab's REST-API using Owner00's access token.
6. It exctract the running GitLab's data to the local file system. This is done using docker to zip up GitLab's datafiles on the run GitLab container, and then using Docker's cp command to copy the zip files out of the container and to a local directory (location is hard-coded).
7. It shuts down GitLab. This is done using Docker.
8. It creates the GitLab-Kitainer. This is done using a Dockerfile and Docker. The data files are copied into the image along with a script that is ran when the container is started. The script extracts the data into the correct locations in the image and then starts GitLab as normal.

TODO: There are many hard-coded values that should be turned into a parameter. A parameterization technique must be chosen (e.g., command-line parameters, configuration files, environment variables, all of the above.).

## Setting the Root Password

GitLab uses the GITLAB_ROOT_PASSWORD environment variable to set the initial root password on installation (see <https://docs.gitlab.com/ce/administration/environment_variables.html>). The GitLab omnibus image finishes its installation on first run. GitLab-Kitainerize sets this environment variable in a docker-compose.yml file, so when docker-compose is used to start GitLab, it passes this variable to GitLab, then GitLab uses its value to initialize the root password (see <https://docs.gitlab.com/omnibus/docker/> for how to work with GitLab with docker-compose).

## Getting Access Tokens

Currently (2/2/2020) the only way to get an access token is using GitLab's UI. This makes it difficult to automate any process that needs to do so. GitLab-Kitainerize needs access tokens to use GitLab's REST-API to create accounts and import projects. To automate this, GitLab-Kitainerize needs to interact with the GitLab UI to get access tokens in an automated way.

There is an open issue for this feature in GitLab's issue tracker (<https://gitlab.com/gitlab-org/gitlab/issues/17176>). This feature is currently scheduled for 12.8. There you can also find a link to another project that provides a Python script that uses Selenium through Chrome to interact with GitLab's UI to get access tokens (<https://gitlab.com/khair1/get-gitlab-token/tree/master>). This is meant as a temporary fix until a more permanent solution is create in 12.8. GitLab-Kitainerize currently uses a slightly customized version of this script. As such it has the following dependencies.

* Python3
* Chrome
* Chrome WebDriver
* Python3 Selenium

These dependencies increase the footprint of GitLab-Kitainerize and makes it more complicated to use because these dependencies must be installed first.

TODO: If and when GitLab implements a REST-API interface for getting access tokens, this part of GitLab-Kitainerize can be re-implemented and these dependencies can be removed.

TODO: Another unresolved issue here is that Chrome is not open-source. Ideally, we'd like all of GitLab-Kitainerize's dependencies to be open-source. The obvious move is to use FireFox instead.

## Creating User Accounts

GitLab-Kitainerize creates the Owner00 account using GitLab's REST-API, using curl.

## Importing Projects

GitLab-Kitainerize imports projects through GitLab's REST-API, using curl.

## Persisting GitLab Data

GitLab-Kitainerize uses Docker to run commands inside the running GitLab container to zip up the data directories, and then to copy out those files.

So far this appears to work; although, there is a risk that these files are not entirely stable in a running instance. If this ever becomes a problem, we'll need to find another way.

There is/was another way that did not work well on MacOS: volume mounting. The documentation for using GitLab images demonstrate how to mount volumes. These did not work in our initial tests; some permission issue on MacOS. So we quickly abandoned this approach in favor of copying the data out of the live instance. However, this would likely be more stable as we could shut down the live instance and the local directories that were mounted would retain the data. If we could solve the MacOS volume mounting issue, this might be a better alternative.
